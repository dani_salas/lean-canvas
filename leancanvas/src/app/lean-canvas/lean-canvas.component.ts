import { Component, Inject } from '@angular/core';
import {MatDialog} from '@angular/material';
import { InsertDialogComponent } from '../insert-dialog/insert-dialog.component';
import { DomSanitizer, } from '@angular/platform-browser';

export interface Tile {
  color: string;
  cols: number;
  rows: number;
  text: string;
  id: number;
  style: number;
}
export interface Data{
  id:number;
  rows: string[];
  enable: boolean;
}
@Component({
  selector: 'lean-canvas',
  templateUrl: './lean-canvas.component.html',
  styleUrls: ['./lean-canvas.component.css']
})
export class LeanCanvasComponent{

  uri : object;
  height = 200;
  multiplier  = 2.3;

  data : Data[] = [
      {id:0,rows:[],enable:true},
      {id:1,rows:[],enable:false},
      {id:2,rows:[],enable:false},
      {id:3,rows:[],enable:false},
      {id:4,rows:[],enable:true},
      {id:5,rows:[],enable:false},
      {id:6,rows:[],enable:false},
      {id:7,rows:[],enable:false},
      {id:8,rows:[],enable:false},
  ];
  tiles: Tile[] = [
    {text: 'Problem', cols: 1, rows: 4, color: 'white',id:0,style:this.height*this.multiplier},
    {text: 'Solution', cols: 1, rows: 2, color: 'white',id:1,style:this.height},
    {text: 'Unique Value Proposition ', cols: 1, rows: 4, color: 'white',id:2,style:this.height*this.multiplier},
    {text: 'Unfair Advantage', cols: 1, rows: 2, color: 'white',id:3,style:this.height},
    {text: 'Customer Segments', cols: 1, rows: 4, color: 'white',id:4,style:this.height*this.multiplier},
    {text: 'Key Metrics', cols: 1, rows: 2, color: 'white',id:5,style:this.height},
    {text: 'Channels', cols: 1, rows: 2, color: 'white',id:6,style:this.height},
    {text: 'Cost Structure', cols: 3, rows: 2, color: 'white',id:7,style:this.height},
    {text: 'Revenue Streams', cols: 2, rows: 2, color: 'white',id:8,style:this.height},
  ];
  constructor(public dialog: MatDialog,private sanitizer: DomSanitizer) { 
    this.checkData();
  }

  print(text){
    console.log(text);
  }

  insertData(id,row){
    if(this.data[id].enable){

      const dialogRef = this.dialog.open(InsertDialogComponent, {
        width: '400px',
        height: '400px',
        data: {id: id}
      });
  
      dialogRef.afterClosed().subscribe(result => {
        if(result.row.trim().length > 0 ){
          this.data[id].rows.push(result.row);
          this.checkData();
        }
      });
    }
    
  }
 
  deleteData(id,row){
    var index = this.data[id].rows.findIndex((data)=>row === data);
    this.data[id].rows.splice(index,1);
    this.checkData();
  }

  checkData(){
    if(this.data[0].rows.length>0 && this.data[4].rows.length>0){
      this.data[2].enable = true;
    }
    else{
      this.data[2].enable = false;
    }
    if(this.data[1].rows.length>0){
      this.data[6].enable = true;
    }else{
      this.data[6].enable = false;
    }
    if(this.data[2].rows.length>0){
      this.data[1].enable = true;
    }else{
      this.data[1].enable = false;
    }
    if(this.data[6].rows.length>0){
      this.data[7].enable = true;
      this.data[8].enable = true;
    }else{

      this.data[7].enable = false;
      this.data[8].enable = false;
    }
    if(this.data[7].rows.length>0 && this.data[8].rows.length>0){
      this.data[5].enable = true;
    }
    else{
      this.data[5].enable = false;
    }
    if(this.data[5].rows.length>0){
      this.data[3].enable = true;
    }else{
      this.data[3].enable = false;
    }
    this.generateDownloadJsonUri();
  }


  openDialog(): void {
    const dialogRef = this.dialog.open(InsertDialogComponent, {
      width: '400px',
      // data: {name: this.name, animal: this.animal}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }
  generateDownloadJsonUri() {
    var theJSON = JSON.stringify(this.data);
    this.uri = this.sanitizer.bypassSecurityTrustUrl("data:text/json;charset=UTF-8," + encodeURIComponent(theJSON));
}



}
